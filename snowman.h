#ifndef SNOWMAN_H
#define SNOWMAN_H

#include <QPainter>

// ( a + b ) / a = a / b
constexpr double goldenRatio = 1.618033988749894;

class Snowman
{
public:
    // constructors intentionally not constexpr, because then it needs to be inlined
    Snowman(int width, int height);

    void paintHead(QPainter& painter) const;
    void paintUpperBody(QPainter& painter) const;
    void paintLowerBody(QPainter& painter) const;
    void paintArms(QPainter& painter) const;
    void paintButtons(QPainter& painter) const;
    void paintFace(QPainter& painter) const;

private:
    void paintEyes(QPainter& painter) const;
    void paintMouth(QPainter& painter) const;

    int centerX;
    int topRadius;
    int topCenterY;
    int middleRadius;
    int middleCenterY;
    int bottomRadius;
    int bottomCenterY;
};

#endif // SNOWMAN_H
