#include <stdexcept>
#include <array>
#include <random>
#include "game.h"

namespace  {

const std::array<std::string, 18> WORDS{
    "ALGOL", "BASIC", "COBOL", "DART", "EIFFEL",
    "FORTRAN", "GO", "GROOVY", "JAVA", "JAVASCRIPT",
    "LOGO", "MODULA", "PASCAL", "RUST", "SCALA",
    "SMALLTALK", "SWIFT", "TYPESCRIPT"
};

} // unnamed namespace

Game::Game(std::string word, int maxNoOfGuesses)
    : charGuesser_{std::move(word)}
    , maxNoOfGuesses_{maxNoOfGuesses}
{
    if (maxNoOfGuesses_ <= 0) {
        throw std::invalid_argument("maxNoOfGuesses must be >= 0 !");
    }
}

bool Game::lost() const
{
    return wrongGuesses_ >= maxNoOfGuesses_;
}

bool Game::over() const
{
    return lost() || won();
}

bool Game::won() const
{
    return charGuesser_.allCharsRevealed();
}

bool Game::guessCharacter(char guess)
{
    int foundChars = charGuesser_.guessCharacter(guess);
    bool guessedCorrectly = foundChars != 0;
    if (! guessedCorrectly) {
        ++wrongGuesses_;
    }
    return guessedCorrectly;
}

std::string Game::revealedCharsWithSpaces() const
{
    // I could initialiue the string with (2*length - 1),
    // but that'S not necessary because of the small string optimization anyway
    std::string result{};
    bool first = true;
    const std::string revealedChars = charGuesser_.revealedChars();
    for (auto iter = revealedChars.cbegin(); iter != revealedChars.cend(); ++iter) {
        if (first) {
            first = false;
        } else {
            result.push_back(' ');
        }
        result.push_back(*iter);
    }
    return result;
}

std::string Game::getRandomWord()
{
    const int range_from  = 0;
    const int range_to    = WORDS.size();
    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int> distr(range_from, range_to);
    int idx = distr(generator);
    return WORDS.at(idx);
}
