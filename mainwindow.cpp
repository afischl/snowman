#include <QKeyEvent>
#include <QPainter>
#include "mainwindow.h"
#include "snowman.h"

namespace  {

constexpr int MAX_NO_OF_GUESSES = 6;

double getPenSize(int height)
{
    double pensize{height / 200.0};
    return pensize < 1.0 ? 1.0 : pensize;
}

} // unnamed namespace

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , game{Game::getRandomWord(), MAX_NO_OF_GUESSES}
{
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);
}

void MainWindow::paintEvent(QPaintEvent*)
{
    const QRect localGeometry = this->geometry();

    QPainter painter{this};
    painter.setRenderHint(QPainter::Antialiasing, true);
    QPen pen{Qt::black, getPenSize(localGeometry.height())};
    painter.setPen(pen);

    const int topPart = localGeometry.height() / goldenRatio;

    const Snowman snowman{localGeometry.width(), topPart};
    int wrongGuesses = game.lost() ? MAX_NO_OF_GUESSES : game.wrongGuesses();
    // intentional fall-through
    switch (wrongGuesses) {
        case 6: snowman.paintFace(painter);
        case 5: snowman.paintArms(painter);
        case 4: snowman.paintButtons(painter);
        case 3: snowman.paintHead(painter);
        case 2: snowman.paintUpperBody(painter);
        case 1: snowman.paintLowerBody(painter);
    }

    const int bottomPart = localGeometry.height()-topPart;
    QFont font = painter.font();
    font.setPixelSize(bottomPart/5);
    painter.setFont(font);
    QRect textRect{0, topPart, localGeometry.width(), bottomPart};
    painter.drawText(textRect, Qt::AlignVCenter | Qt::AlignHCenter, getText());
}

void MainWindow::keyReleaseEvent(QKeyEvent *event) {
    if (game.over()) {
        game = Game{Game::getRandomWord(), MAX_NO_OF_GUESSES};
    } else {
        int key = event->key();
        if (key >= 'A' && key <= 'Z') {
            char c = static_cast<char>(key);
            game.guessCharacter(c);
        }
    }
    repaint();
}

QString MainWindow::getText() const
{
    std::string text;
    if (game.won()) {
        text = "Yes, the word was " + game.wordToGuess() + ".\nYou won!\n\nPress any key to play again.";
    } else if (game.lost()) {
        text = "You lost.\nThe word was " + game.wordToGuess() + ".\n\nPress any key to play again.";
    } else {
        text = game.revealedCharsWithSpaces();
    }
    return QString::fromStdString(text);
}
