#ifndef CHARGUESSER_H
#define CHARGUESSER_H

#include <string>

class CharGuesser
{
public:
    CharGuesser(std::string word, char placeholder = '_');

    int guessCharacter(char guess);
    bool allCharsRevealed() const;

    inline std::string charsToFind() const;
    inline std::string revealedChars() const;

private:
    std::string charsToFind_;
    std::string revealedChars_;

};


// inline:

std::string CharGuesser::charsToFind() const
{
    return charsToFind_;
}

std::string CharGuesser::revealedChars() const
{
    return revealedChars_;
}

#endif // CHARGUESSER_H
