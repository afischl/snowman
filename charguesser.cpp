#include "charguesser.h"

CharGuesser::CharGuesser(std::string word, char placeholder)
    : charsToFind_{std::move(word)}
    , revealedChars_(charsToFind_.length(), placeholder)
{
}

int CharGuesser::guessCharacter(char guess)
{
    int foundChars = 0;
    for (size_t i=0; i<charsToFind_.length(); ++i) {
        if (charsToFind_.at(i) == guess) {
            ++foundChars;
            revealedChars_.at(i) = guess;
        }
    }
    return foundChars;
}

bool CharGuesser::allCharsRevealed() const
{
    return charsToFind_ == revealedChars_;
}

