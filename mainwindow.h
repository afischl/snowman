#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "game.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() = default;

protected:
    void paintEvent(QPaintEvent* event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    QString getText() const;

    Game game;
};

#endif // MAINWINDOW_H
