#include "mainwindow.h"

#include <QApplication>
#include <QScreen>

// this has no effect when compiled to WebAssembly
void resizeMainWindow(const QApplication& a, MainWindow& w)
{
    QScreen* screen = a.primaryScreen();
    QSize availableSize = screen->availableGeometry().size();
    w.resize(availableSize * 0.7);
}

int main(int argc, char *argv[])
{
    QApplication a{argc, argv};
    MainWindow w;
    resizeMainWindow(a, w);
    w.show();
    return a.exec();
}
