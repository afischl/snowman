#ifndef GAME_H
#define GAME_H

#include "charguesser.h"

class Game
{
public:
    Game(std::string word, int maxNoOfGuesses);

    inline int wrongGuesses() const;
    inline std::string wordToGuess() const;
    bool lost() const;
    bool over() const;
    bool won() const;
    std::string revealedCharsWithSpaces() const;
    bool guessCharacter(char guess);

    static std::string getRandomWord();

private:
    CharGuesser charGuesser_;
    int maxNoOfGuesses_;
    int wrongGuesses_ = 0;
};


// inline:

std::string Game::wordToGuess() const
{
    return charGuesser_.charsToFind();
}

int Game::wrongGuesses() const
{
    return wrongGuesses_;
}

#endif // GAME_H
