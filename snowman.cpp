#include <cmath>
#include <stdexcept>
#include "snowman.h"

namespace {

constexpr double pi = 3.141592653589793238462643383279502884;

constexpr double angleInRad = -pi/5;

void drawCircle(QPainter& painter, int centerX, int centerY, int radius, QBrush brush = Qt::BrushStyle::NoBrush)
{
    QPoint center{centerX, centerY};
    painter.setBrush(brush);
    painter.drawEllipse(center, radius, radius);
}

} // unnamed namespace

Snowman::Snowman(int width, int height)
    : centerX{width/2}
    , topRadius{static_cast<int>((height / (2 + goldenRatio + goldenRatio*goldenRatio)) / 2)}
    , topCenterY{2*topRadius}
    , middleRadius{static_cast<int>(topRadius*goldenRatio)}
    , middleCenterY{topCenterY + topRadius + middleRadius}
    , bottomRadius{static_cast<int>(middleRadius*goldenRatio)}
    , bottomCenterY{middleCenterY + middleRadius + bottomRadius}
{
    if (width <= 0 || height <= 0) {
        throw std::invalid_argument("width and height must be >= 0 !");
    }
}

void Snowman::paintHead(QPainter &painter) const
{
    drawCircle(painter, centerX, topCenterY, topRadius);
}

void Snowman::paintUpperBody(QPainter &painter) const
{
    drawCircle(painter, centerX, middleCenterY, middleRadius);
}

void Snowman::paintLowerBody(QPainter &painter) const
{
    drawCircle(painter, centerX, bottomCenterY, bottomRadius);
}

void Snowman::paintArms(QPainter &painter) const
{
    const int x = middleRadius * std::cos(angleInRad);
    const int y = middleRadius * std::sin(angleInRad);
    painter.drawLine(centerX + x, middleCenterY + y, centerX + 3*x, middleCenterY + 2*y);
    painter.drawLine(centerX - x, middleCenterY + y, centerX - 3*x, middleCenterY + 2*y);
}

void Snowman::paintButtons(QPainter &painter) const
{
    const int buttonRadius = middleRadius / 10;
    drawCircle(painter, centerX, middleCenterY, buttonRadius, Qt::BrushStyle::SolidPattern);
    drawCircle(painter, centerX, middleCenterY - middleRadius/2, buttonRadius, Qt::BrushStyle::SolidPattern);
    drawCircle(painter, centerX, middleCenterY + middleRadius/2, buttonRadius, Qt::BrushStyle::SolidPattern);
}

void Snowman::paintFace(QPainter& painter) const
{
    paintEyes(painter);
    paintMouth(painter);
}

void Snowman::paintEyes(QPainter &painter) const
{
    const int eyeRadius = topRadius / 10;
    drawCircle(painter, centerX - topRadius/2, topCenterY - topRadius/4, eyeRadius, Qt::BrushStyle::SolidPattern);
    drawCircle(painter, centerX + topRadius/2, topCenterY - topRadius/4, eyeRadius, Qt::BrushStyle::SolidPattern);
}

void Snowman::paintMouth(QPainter &painter) const
{
    const int startAngleIn16thDeg = -30 * 16;
    const int spanAngleIn16thDeg = -120 * 16;
    painter.drawArc(centerX - topRadius/2, topCenterY, topRadius, topRadius/2, startAngleIn16thDeg, spanAngleIn16thDeg);
}
